import datetime
import inspect
import logging
import os
import threading
import time

import dateutil.tz
import requests
from influx_line_protocol import Metric, MetricCollection
from pyHS100 import Discover, SmartPlug

##### Read parameters (Environment variables)
if "TELEGRAF_ADDRESS" in os.environ:
    telegraf_server = os.environ["TELEGRAF_ADDRESS"]
else:
    logging.error(f"TELEGRAF_ADDRESS is not set")

telegraf_port = 8086  # Assume standard port
if "TELEGRAF_PORT" in os.environ:
    telegraf_port = int(os.environ["TELEGRAF_PORT"])

if "HS110_UNITS" in os.environ:
    hs110_units = os.environ["HS110_UNITS"].split(",")
else:
    logging.error(f"HS110_UNITS is not set")

sample_period = 300  # Default sample period is 5 min
if "SAMPLE_PERIOD" in os.environ:
    sample_period = int(os.environ["SAMPLE_PERIOD"])
if sample_period < 60:
    logging.error(f"Do not set sample periods below 60 seconds")


def get_metric_from_plug(plug):
    """Return a line-protocol representation of the supplied plug"""

    metric = Metric("power_consumption")

    tz = dateutil.tz.tzstr(plug.timezone["tz_str"])

    # Timestamp in nanoseconds
    # TODO: Currently the timezone handling is broken in TP-links end, simply
    # # use serverside time for now. Revert once TP-link timestamp is correct!
    
    # epoch_zero = datetime.datetime.utcfromtimestamp(0)
    # epoch = (plug.time - epoch_zero).total_seconds() - tz.utcoffset(
    #     datetime.datetime.now()
    # ).total_seconds()
    epoch_utc = datetime.datetime.utcnow().timestamp()
    metric.with_timestamp(epoch_utc * 1000 * 1000 * 1000)

    metric.add_tag("device", plug.alias)

    # Get information from plug
    emeter = plug.get_emeter_realtime()

    # Add relevant figures to metric
    metric.add_value("power_w", emeter["power_mw"] * 0.001)
    metric.add_value("energy_wh", emeter["total_wh"])
    metric.add_value("voltage_v", emeter["voltage"])
    metric.add_value("current_a", emeter["current_ma"] * 0.001)
    metric.add_value("state", int(plug.is_on))

    return metric


def sample(devices):
    """Will iterate over the supplied list of IP addresses, sample the devices and send the data"""

    print(f"{inspect.stack()[0][3]} called, will check: [{','.join(hs110_units)}]")

    # Common collection for the collected samples
    metrics = MetricCollection()

    # Iterate all expected units
    for unit in hs110_units:
        dev = SmartPlug(unit)

        metrics.append(get_metric_from_plug(dev))

    #
    if metrics:
        print(f"{inspect.stack()[0][3]} collected metrics, send them")
        # Send metrics to telegraf!
        requests.post(
            f"http://{telegraf_server}:{telegraf_port}/telegraf", data=str(metrics)
        )
    else:
        print(f"{inspect.stack()[0][3]} could not collect metrics, nothing to send")

    print(f"{inspect.stack()[0][3]} done")


##### Start periodic calling of sample function
next_call = datetime.datetime.utcnow()


def periodic_call():
    global next_call

    # Print
    print(
        f"{inspect.stack()[0][3]} called at UTC: {datetime.datetime.utcnow()} (next_call: {next_call})"
    )

    # Set up next call
    next_call = next_call + datetime.timedelta(seconds=sample_period)
    next_call_in_s = (next_call - datetime.datetime.utcnow()).total_seconds()

    t = threading.Timer(next_call_in_s, periodic_call)
    t.start()

    print(f"{inspect.stack()[0][3]} next call at {next_call} (in {next_call_in_s} s)")

    # Run this call
    sample(hs110_units)


##### Execute first periodic call
periodic_call()
